/**
 * oscP5sendreceive by andreas schlegel
 * example shows how to send and receive osc messages.
 * oscP5 website at http://www.sojamo.de/oscP5
 */
 
import oscP5.*;
import netP5.*;

// slider
import controlP5.*;
ControlP5 cp5;
int sliderValue = 100;
  
OscP5 oscP5;
//NetAddress myRemoteLocation;

void setup() {
  // slider
  cp5 = new ControlP5(this);
  // add a horizontal sliders, the value of this slider will be linked
  // to variable 'sliderValue' 
  cp5.addSlider("slider")
     .setPosition(100,50)
     .setRange(0,70)
     .setValue(0)
     ;
     
  cp5.getController("slider").setValue(42);
  
  size(400,400);
  frameRate(25);
  /* start oscP5, listening for incoming messages at port 12000 */
  oscP5 = new OscP5(this,4242);
  
  /* myRemoteLocation is a NetAddress. a NetAddress takes 2 parameters,
   * an ip address and a port number. myRemoteLocation is used as parameter in
   * oscP5.send() when sending osc packets to another computer, device, 
   * application. usage see below. for testing purposes the listening port
   * and the port of the remote location address are the same, hence you will
   * send messages back to this sketch.
   */
  //myRemoteLocation = new NetAddress("127.0.0.1",4242);
}


void draw() {
  background(0);  
//  println("--");
}

//void mousePressed() {
//  /* in the following different ways of creating osc messages are shown by example */
//  OscMessage myMessage = new OscMessage("/youpi");
//  
//  myMessage.add(123); /* add an int to the osc message */
//
//  /* send the message */
//  oscP5.send(myMessage, myRemoteLocation); 
//}


///* incoming osc message are forwarded to the oscEvent method. */
//void oscEvent(OscMessage theOscMessage) {
//  /* print the address pattern and the typetag of the received OscMessage */
//  print("### received an osc message.");
//  print(" addrpattern: "+theOscMessage.addrPattern());
//  println(" typetag: "+theOscMessage.typetag());
//}

float emg_min = 10000;
float emg_max = -1;
float emg_value = 0;
int emg_id = 0;
float[] emg_pass = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
void set_emg_value(float val) {
	float v = 0;

	emg_pass[emg_id] = val;
	for (int i = 0; i < emg_pass.length; i++)
	{
		v += emg_pass[i];
	}
	emg_id = (emg_id + 1) % emg_pass.length;
	v /= emg_pass.length;

	cp5.getController("slider").setValue(v);

	if (v > emg_max) {
		emg_max = v;
	}
	if (v < emg_min) {
		emg_min = v;
	}
	println("\n\n=============================");
	//println("emg: ", emg[0], emg[1], emg[2], emg[3], emg[4], emg[5], emg[6], emg[7]);
	println("val: ", v);
	println("min: ", emg_min);
	println("max: ", emg_max);
}


/* incoming osc message are forwarded to the oscEvent method. */
void oscEvent(OscMessage theOscMessage) {
  /* check if theOscMessage has the address pattern we are looking for. */
  int[] emg = {0, 0, 0, 0, 0, 0, 0, 0};
  float average = 0;

  if(theOscMessage.checkAddrPattern("/emg")==true) {
    /* check if the typetag is the right one. */
    if(theOscMessage.checkTypetag("iiiiiiii")) {
    	/* parse theOscMessage and extract the values from the osc message arguments. */
		for (int i = 0; i < 8; i++) {
		    emg[i] = abs(theOscMessage.get(i).intValue());
		    average += emg[i];
		}
		average /= 8.;
		set_emg_value(average);

//      int firstValue = theOscMessage.get(0).intValue();
//      float secondValue = theOscMessage.get(1).floatValue();
//      String thirdValue = theOscMessage.get(2).stringValue();
//      print("### received an osc message /test with typetag ifs.");
      return;
    }
  }
  println("### received an osc message. with address pattern "+theOscMessage.addrPattern());
}
