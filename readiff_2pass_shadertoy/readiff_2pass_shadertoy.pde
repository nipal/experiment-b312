// Reaction Diffusion - 2 Pass
// By Shane
// https://www.shadertoy.com/view/XsG3z1
// Ported to Processing by Kazik Pogoda


import org.openkinect.freenect.*;
import org.openkinect.processing.*;

KinectTracker tracker;
Kinect kinect;


PShader bufferAShader;
PShader imageShader;
PGraphics bufferA;


float previousTime = 0.0;

PVector lastMousePosition;
boolean mouseDragged = false;
float mouseClickState = 0.0;

void setup() {
  //size(840, 660, P2D);
  fullScreen(P2D);
  
  // Initiate Kinect
  kinect = new Kinect(this);
  tracker = new KinectTracker();
  
  bufferA = createGraphics(width, height, P2D);

  // Load the shader files from the "data" folder
  bufferAShader = loadShader("bufferA.glsl");
  imageShader = loadShader("image.glsl");
  
  // We assume the dimension of the window will not change over time, 
  // therefore we can pass its values in the setup() function  
  bufferAShader.set("iResolution", float(width), float(height), 0.0);
  imageShader.set("iResolution", float(width), float(height), 0.0);
  bufferAShader.set("iChannel0", bufferA); // feedback loop
  imageShader.set("iChannel0", bufferA);

}


void draw() {
  // ============= Kinect part =================
  // Run the tracking analysis
  tracker.track();
  // Set Up the distance avrage Kinect
  int t = 860;
  tracker.setThreshold(t);
  PVector Point_tracking = tracker.getLerpedPos();
  
  // shader playback time (in seconds)
  float currentTime = millis()/1000.0;
  //  bufferAShader.set("iTime", currentTime);
  //  imageShader.set("iTime", currentTime);


  // When the average point is inside
  if (tracker.Isinside()) {
    //lastMousePosition.set(Point_tracking.x, height - Point_tracking.y);
    mouseClickState = 1.0;
  } else {
    mouseClickState = 0.0;
  }
  
  // Touch the shadertoy (: Il faut set le x et le y par rapport a l'ecran)
  //  bufferAShader.set( "iMouse", mouse.x, mouse.y, mouseClickState, mouseClickState);
  bufferAShader.set( "iMouse", 2*Point_tracking.x, (height - Point_tracking.y), mouseClickState, mouseClickState);

  // shader playback frame
  bufferAShader.set("iFrame", frameCount);

  // draw bufferA first
  bufferA.beginDraw();
  bufferA.shader(bufferAShader);
  bufferA.rect(0, 0, width, height);
  bufferA.endDraw();

  // Apply the specified shader to any geometry drawn from this point  
  shader(imageShader);

  // Draw the output of the shader onto a rectangle that covers the whole viewport.
  rect(0, 0, width, height);

}
