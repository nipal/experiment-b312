#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

// From Processing 2.1 and up, this line is optional
#define PROCESSING_COLOR_SHADER

// if you are using the filter() function, replace the above with
// #define PROCESSING_TEXTURE_SHADER

// ----------------------
//  SHADERTOY UNIFORMS  -
// ----------------------

uniform vec3      iResolution;           // viewport resolution (in pixels)
//uniform float     iTime;                 // shader playback time (in seconds) (replaces iGlobalTime which is now obsolete)
uniform int       iFrame;                // shader playback frame
uniform vec4      iMouse;                // mouse pixel coords. xy: current (if MLB down), zw: click

uniform sampler2D iChannel0;
uniform sampler2D iChannel1;
uniform sampler2D iChannel2;


void mainImage( out vec4 fragColor, in vec2 fragCoord );

void main() {
  mainImage(gl_FragColor,gl_FragCoord.xy);
}

// ------------------------------
//  SHADERTOY CODE BEGINS HERE  -
// ------------------------------


//Conventions:
// x component = outer radius / ring
// y component = inner radius / disk
/*
   _
 /   \
|  O  |
 \ _ /
*/
const float PI = 3.14159265;
const float dt = 0.30;


const vec2 r = vec2(10.0, 3.0);

// SmoothLifeL rules
const float b1 = 0.257;
const float b2 = 0.336;
const float d1 = 0.365;
const float d2 = 0.549;

const float alpha_n = 0.028;
const float alpha_m = 0.147;
/*------------------------------*/

//const float KEY_LEFT  = 37.5/256.0;
const float KEY_UP    = 38.5/256.0;
//const float KEY_RIGHT = 39.5/256.0;
const float KEY_DOWN  = 40.5/256.0;
const float KEY_SPACE  = 32.5/256.0;


// 1 out, 3 in... <https://www.shadertoy.com/view/4djSRW>
#define MOD3 vec3(.1031,.11369,.13787)
float hash13(vec3 p3) {
	p3 = fract(p3 * MOD3);
    p3 += dot(p3, p3.yzx+19.19);
    return fract((p3.x + p3.y)*p3.z);
}


/* ---------------- Sigmoid functions ------------------------------------ */

// TODO: reduce unnecessary parameters (remove arguments, use global consts)

float sigmoid_a(float x, float a, float b) {
    return 1.0 / (1.0 + exp(-(x - a) * 4.0 / b));
}

// unnecessary 
float sigmoid_b(float x, float b, float eb) {
    return 1.0 - sigmoid_a(x, b, eb);
}

float sigmoid_ab(float x, float a, float b, float ea, float eb) {
    return sigmoid_a(x, a, ea) * sigmoid_b(x, b, eb);
}

float sigmoid_mix(float x, float y, float m, float em) {
    return x * (1.0 - sigmoid_a(m, 0.5, em)) + y * sigmoid_a(m, 0.5, em);
}

/* ----------------------------------------------------------------------- */

// SmoothLifeL
float transition_function(vec2 disk_ring) {
    return sigmoid_mix(sigmoid_ab(disk_ring.x, b1, b2, alpha_n, alpha_n),
                       sigmoid_ab(disk_ring.x, d1, d2, alpha_n, alpha_n), disk_ring.y, alpha_m
                      );
}

// unnecessary (?)
float ramp_step(float steppos, float t) {
    return clamp(t-steppos+0.5, 0.0, 1.0);
}

// unnecessary
vec2 wrap(vec2 position) { return fract(position); }

// Computes both inner and outer integrals
// TODO: Optimize. Much redundant computation. Most expensive part of program.
vec2 convolve(vec2 uv) {
    vec2 result = vec2(0.0);
    for (float dx = -r.x; dx <= r.x; dx++) {
        for (float dy = -r.x; dy <= r.x; dy++) {
            vec2 d = vec2(dx, dy);
            float dist = length(d);
            vec2 offset = d / iResolution.xy;
            vec2 samplepos = wrap(uv + offset);
            //if(dist <= r.y + 1.0) {
                float weight = texture(iChannel0, samplepos).x;
            	result.x += weight * ramp_step(r.y, dist) * (1.0-ramp_step(r.x, dist));	
            	
            //} else if(dist <= r.x + 1.) {
                //float weight = texture(iChannel0, uv+offset).x;
				result.y += weight * (1.0-ramp_step(r.y, dist));
            //}
        }
    }
    return result;
}





void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    vec3 color = vec3(0.0);
    
    vec2 uv = fragCoord.xy / iResolution.xy;
    
    // Compute inner disk and outer ring area.
    vec2 area = PI * r * r;
    area.x -= area.y;
    /* -------------------------------------*/
    
    // TODO: Cleanup.
    color = texture(iChannel0, uv).xyz;
    vec2 normalized_convolution = convolve(uv.xy).xy / area;
    color.x = color.x + dt * (2.0 * transition_function(normalized_convolution) - 1.0);
    color.yz = normalized_convolution;
    color = clamp(color, 0.0, 1.0);
    
    // Set initial conditions. TODO: Move to function / cleanup
    if(iFrame < 10 || texture( iChannel2, vec2(KEY_SPACE,0.5) ).x > 0.5) {
        color = vec3(hash13(vec3(fragCoord, iFrame)) - texture(iChannel1, uv).x + 0.5);
    }
    
    if(iMouse.z > 0.) {
        //vec2 dst = abs(uv - iMouse.xy/iResolution.xy);
        float dst = length((fragCoord.xy - iMouse.xy)/iResolution.xx);
        /*if(max(dst.x * iResolution.x/iResolution.y, dst.y) < 0.05) {
        	color = vec3(hash13(vec3(fragCoord, iFrame)) - texture(iChannel1, uv).x + 0.5);
        }*/
        if(dst <= (r.x)/iResolution.x) {
        	color.x = step((r.y+1.5)/iResolution.x, dst) * (1.0 - step(r.x/iResolution.x, dst));
        }
        /*if(dst <= (r.x)/iResolution.x) {
        	color.x = step((r.y+1.0)/iResolution.x, dst) * (1.0 - step((r.x-0.5)/iResolution.x, dst));
        }*/
    }
    
    // Inspect transition function. TODO: Move to function / ifdef
    if(texture( iChannel2, vec2(KEY_DOWN, 5.0/3.0) ).x > 0.5) {
        color = vec3(transition_function(uv));
    }
    
    if(texture( iChannel2, vec2(KEY_UP, 0.5)).x > 0.5) {
    	color = vec3(0.0);
    }
    
    fragColor = vec4(color, 1.0);
}

///// // Reaction-diffusion pass.
///// //
///// // Here's a really short, non technical explanation:
///// //
///// // To begin, sprinkle the buffer with some initial noise on the first few frames (Sometimes, the
///// // first frame gets skipped, so you do a few more).
///// //
///// // During the buffer loop pass, determine the reaction diffusion value using a combination of the
///// // value stored in the buffer's "X" channel, and a the blurred value - stored in the "Y" channel
///// // (You can see how that's done in the code below). Blur the value from the "X" channel (the old
///// // reaction diffusion value) and store it in "Y", then store the new (reaction diffusion) value
///// // in "X." Display either the "X" value  or "Y" buffer value in the "Image" tab, add some window
///// // dressing, then repeat the process. Simple... Slightly confusing when I try to explain it, but
///// // trust me, it's simple. :)
///// //
///// // Anyway, for a more sophisticated explanation, here are a couple of references below:
///// //
///// // Reaction-Diffusion by the Gray-Scott Model - http://www.karlsims.com/rd.html
///// // Reaction-Diffusion Tutorial - http://www.karlsims.com/rd.html
///// 
///// // Cheap vec2 to vec3 hash. Works well enough, but there are other ways.
///// vec3 hash33(in vec2 p){
/////   float n = sin(dot(p, vec2(41, 289)));
/////   return fract(vec3(2097152, 262144, 32768)*n);
///// }
///// 
///// // Serves no other purpose than to save having to write this out all the time. I could write a
///// // "define," but I'm pretty sure this'll be inlined.
///// vec4 tx(in vec2 p){ return texture(iChannel0, p); }
///// 
///// // Weighted blur function. Pretty standard.
///// float blur(in vec2 p){
///// 
/////   // Used to move to adjoining pixels. - uv + vec2(-1, 1)*px, uv + vec2(1, 0)*px, etc.
/////   vec3 e = vec3(1, 0, -1);
/////   vec2 px = 1./iResolution.xy;
///// 
/////   // Weighted 3x3 blur, or a cheap and nasty Gaussian blur approximation.
/////   float res = 0.0;
/////   // Four corners. Those receive the least weight.
/////   res += tx(p + e.xx*px ).x + tx(p + e.xz*px ).x + tx(p + e.zx*px ).x + tx(p + e.zz*px ).x;
/////   // Four sides, which are given a little more weight.
/////   res += (tx(p + e.xy*px ).x + tx(p + e.yx*px ).x + tx(p + e.yz*px ).x + tx(p + e.zy*px ).x)*2.;
/////   // The center pixel, which we're giving the most weight to, as you'd expect.
/////   res += tx(p + e.yy*px ).x*4.;
/////   // Normalizing.
/////   return res/16.;
///// 
///// }
///// 
///// // The reaction diffusion loop.
///// //
///// void mainImage( out vec4 fragColor, in vec2 fragCoord ){
///// 
///// 
/////   vec2 uv = fragCoord/iResolution.xy; // Screen coordinates. Range: [0, 1]
/////   vec2 pw = 1./iResolution.xy; // Relative pixel width. Used for neighboring pixels, etc.
///// 
///// 
/////   // The blurred pixel. This is the result that's used in the "Image" tab. It's also reused
/////   // in the next frame in the reaction diffusion process (see below).
/////   float avgReactDiff = blur(uv);
///// 
///// 
/////   // The noise value. Because the result is blurred, we can get away with plain old static noise.
/////   // However, smooth noise, and various kinds of noise textures will work, too.
/////   vec3 noise = hash33(uv + vec2(53, 43)*iTime)*.6 + .2;
///// 
/////   // Used to move to adjoining pixels. - uv + vec2(-1, 1)*px, uv + vec2(1, 0)*px, etc.
/////   vec3 e = vec3(1, 0, -1);
///// 
/////   // Gradient epsilon value. The "1.5" figure was trial and error, but was based on the 3x3 blur radius.
/////   vec2 pwr = pw*1.5;
///// 
/////   // Use the blurred pixels (stored in the Y-Channel) to obtain the gradient. I haven't put too much
/////   // thought into this, but the gradient of a pixel on a blurred pixel grid (average neighbors), would
/////   // be analogous to a Laplacian operator on a 2D discreet grid. Laplacians tend to be used to describe
/////   // chemical flow, so... Sounds good, anyway. :)
/////   //
/////   // Seriously, though, take a look at the formula for the reacion-diffusion process, and you'll see
/////   // that the following few lines are simply putting it into effect.
///// 
/////   // Gradient of the blurred pixels from the previous frame.
/////   vec2 lap = vec2(tx(uv + e.xy*pwr).y - tx(uv - e.xy*pwr).y, tx(uv + e.yx*pwr).y - tx(uv - e.yx*pwr).y);//
///// 
/////   // Add some diffusive expansion, scaled down to the order of a pixel width.
/////   uv = uv + lap*pw*3.0;
///// 
/////   // Stochastic decay. Ie: A differention equation, influenced by noise.
/////   // You need the decay, otherwise things would keep increasing, which in this case means a white screen.
/////   float newReactDiff = tx(uv).x + (noise.z - 0.5)*0.0025 - 0.002;
///// 
/////   // Reaction-diffusion.
/////   newReactDiff += dot(tx(uv + (noise.xy-0.5)*pw).xy, vec2(1, -1))*0.145;
///// 
///// 
/////   // Storing the reaction diffusion value in the X channel, and avgReactDiff (the blurred pixel value)
/////   // in the Y channel. However, for the first few frames, we add some noise. Normally, one frame would
/////   // be enough, but for some weird reason, it doesn't always get stored on the very first frame.
/////   if(iFrame>9) fragColor.xy = clamp(vec2(newReactDiff, avgReactDiff/.98), 0., 1.);
/////   else fragColor = vec4(noise, 1.);
///// 
///// }


// ----------------------------
//  SHADERTOY CODE ENDS HERE  -
// ----------------------------
